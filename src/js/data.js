const data = {
    "first_name": "Dmitry",
    "last_name": "Lomanets",
    "job_title": "Frontend-разработчик",
    "photo": "images/my_photo.jpg",
    "phone": "+79912431202",
    "email": "dimalomanec@gmail.ru",
    "skype": "dmitrylomanets",
    "linkedin": "link_to_linkedin",
    "address": "пл. Победы 55, Калининград",
    "skills": [
        "HTML, CSS, JS",
        "Web design",
        "Figma",
        "Adobe Creative Cloud",
        "Microsoft Office",
    ],
    "education": [
        ["2013-2016", "«Northern (Arctic) Federal University named after M.V. Lomonosov»"],
    ],
    "achievements": [
        "Marketer of the Year Academic Scholorship Deans List: 2006-2007, National Merit Scholarship Corporation",
    ],
    "profile": "Hi, my name is Dmitry. I am a full stack designer. I can make a website design and make it up. 10 real cases in creating online stores and ledings."
};