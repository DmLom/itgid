const experience = [
    {
        "company": "HelloWorld",
        "city": "Kaliningrad",
        "from": "2021",
        "to": 'Present',
        "position": "teacher",
        "description": "Teaching children to programming. Direction Roblox, Scratch"
    },
    {
        "company": "College of Management",
        "city": "Arkhangelsk",
        "from": "2019",
        "to": '2021',
        "position": "System Administrator",
        "description": "Maintenance of a fleet of computers. Working with the site"
    },
]